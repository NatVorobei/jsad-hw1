// Прототипне наслідування - це можливість одного об'єкта успадковувати властивості і методи іншого об'єкта. Якщо властивість не знайдена в поточному об'єкті, JavaScript автоматично шукає її в його прототипі.
// super() потрібно викликати, щоб виконати батьківський конструктор, інакше об’єкт для this не буде створено.

class Employee{
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name(){
        return this._name;
    }

    set name(name){
        this._name = name;
    }

    get age(){
        return this._age;
    }

    set age(age){
        this._age = age;
    }

    get salary(){
        return this._salary;
    }

    set salary(salary){
        this._salary = salary;
    }
}

let employee1 = new Employee('Nata', 26, 10000);

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this._lang = lang;
    }

    get salary(){
        return super.salary*3;
    }
        
}

let programmer1 = new Programmer('Anna', 20, 7000, 'C#');
let programmer2 = new Programmer('Nikita', 25, 8000, 'C++');
let programmer3 = new Programmer('Mariia', 24, 9000, 'Javascript');

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);

console.log(programmer1.salary)
console.log(programmer2.salary)
console.log(programmer3.salary);
